package repository.music.tests.unit;


import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import repository.music.web.HomeController;

@RunWith(SpringJUnit4ClassRunner.class)
public class HomeControllerTest {

	@Test
	public void shouldReturnSimpleJson() throws Exception {
		
		HomeController controller= new HomeController();
		
		MockMvc mockMvc = standaloneSetup(controller).build();
		
		mockMvc.perform(get("/"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.IsWorking").value(true));
	}

}
