package repository.music.tests.unit;


import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import repository.music.data.Album;
import repository.music.data.Artist;
import repository.music.data.ArtistsRepository;
import repository.music.data.Track;
import repository.music.web.AlbumsController;

@RunWith(SpringJUnit4ClassRunner.class)
public class AlbumsControllerTest {


	private  List<Track> tracks;
	String albumTitle="Metallica";
	private  Album album;
	private  Artist artist;
	String artistName="Metallica";
	
	@Before
	public void initializeTest(){

		tracks= new ArrayList<>();
		Track temp= new Track("Nothing Else Matters");
		Track temp2= new Track("The unforgiven");
		tracks.add(temp);
		tracks.add(temp2);
		album= new Album(albumTitle, tracks.toArray(new Track[2]));
		artist= new Artist(artistName,new Album[]{album});
	}
	@Test
	public void shouldReturnAlbumsList() throws Exception {
		ArtistsRepository repository =mock(ArtistsRepository.class);
		
		AlbumsController controller =new AlbumsController(repository);
		
		MockMvc mockMvc = standaloneSetup(controller).build();
		
		when(repository.findAllAlbumsByArtistName(artistName)).thenReturn(Arrays.asList(album));
		
		mockMvc.perform(get("/artists/Metallica/albums").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0].albumTitle").value("Metallica"))
			.andExpect(jsonPath("$[0].tracks").isArray());
	}
	
	@Test
	public void shouldAddNewAlbum() throws Exception{
		ArtistsRepository repository =mock(ArtistsRepository.class);
		
		AlbumsController controller =new AlbumsController(repository);
		
		MockMvc mockMvc = standaloneSetup(controller).build();
		
		Album newAlbum= new Album("Load", new Track [] {new Track("Cure")});
		when(repository.findArtistByArtistName(artistName)).thenReturn(artist);
		when(repository.findAlbumByArtistNameAndAlbumTitle(artistName, albumTitle)).thenReturn(null);
		when(repository.findArtistByArtistNameAndInsertAlbum(artistName, newAlbum)).thenReturn(newAlbum);
		
		Gson gson = new Gson();
		String json= gson.toJson(newAlbum);
		
		System.out.println();
		System.out.println(json);
		
		mockMvc.perform(post("/artists/Metallica/albums").accept(MediaType.APPLICATION_JSON).contentType(MediaType.APPLICATION_JSON).content(json))
			.andExpect(status().isCreated())
			.andDo(print())
			.andExpect(jsonPath("$.albumTitle").value("Load"))
			.andExpect(jsonPath("$.tracks").isArray());
	}
	
	@Test
	public void shouldReturnOneAlbum() throws Exception {
		
		ArtistsRepository repository = mock(ArtistsRepository.class);
		when(repository.findAlbumByArtistNameAndAlbumTitle(artist.getArtistName(),album.getAlbumTitle())).thenReturn(album);
		
		AlbumsController controller = new AlbumsController(repository);
		
		MockMvc mvc = standaloneSetup(controller).build();
		
		mvc.perform(get("/artists/Metallica/albums/Metallica"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.albumTitle").value("Metallica"));
	}
	
	@Test
	public void shouldReturnModifiedAlbum() throws Exception {
		
		
		Album newAlbum= new Album("Load", new Track [] {new Track("Cure")});
		
		ArtistsRepository repository = mock(ArtistsRepository.class);
		
		when(repository.findAlbumByArtistNameAndAlbumTitle(artistName,albumTitle)).thenReturn(album);
		when(repository.findAlbumByArtistNameAndAlbumTitleAndUpdate(artist.getArtistName(), album.getAlbumTitle(), newAlbum)).thenReturn(newAlbum);
		AlbumsController controller = new AlbumsController(repository);
		
		MockMvc mvc = standaloneSetup(controller).build();
		
		Gson gson = new Gson();
		String json= gson.toJson(newAlbum);
		
		mvc.perform(put("/artists/"+ artist.getArtistName() +"/albums/" + album.getAlbumTitle()).contentType(MediaType.APPLICATION_JSON).content(json))
			.andExpect(status().isOk())
			.andDo(print())
			.andExpect(jsonPath("$.albumTitle").value("Load"));
		
	}

	@Test
	public void shouldDeleteOneEntry() throws Exception {
		
		ArtistsRepository repository = mock(ArtistsRepository.class);
		
		when(repository.findAlbumByArtistNameAndAlbumTitle(artistName, albumTitle)).thenReturn(album);
		doNothing().when(repository).deleteAlbumByArtistNameAndAlbumTitle(artistName, albumTitle);
		
		AlbumsController controller = new AlbumsController(repository);
		
		MockMvc mvc = standaloneSetup(controller).build();
		
		mvc.perform(delete("/artists/Metallica/albums/Metallica").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andDo(print());
		
	}
	
}
