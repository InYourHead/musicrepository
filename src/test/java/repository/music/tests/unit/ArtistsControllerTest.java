package repository.music.tests.unit;


import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.google.gson.Gson;

import repository.music.data.Album;
import repository.music.data.Artist;
import repository.music.data.ArtistsRepository;
import repository.music.data.Track;
import repository.music.web.ArtistsController;

@RunWith(SpringJUnit4ClassRunner.class)
public class ArtistsControllerTest {

	private  List<Track> tracks;
	String albumTitle="Metallica";
	private  Album album;
	private  Artist artist;
	String artistName="Metallica";
	
	@Before
	public void initializeTest(){

		tracks= new ArrayList<>();
		Track temp= new Track("Nothing Else Matters");
		Track temp2= new Track("The unforgiven");
		tracks.add(temp);
		tracks.add(temp2);
		album= new Album(albumTitle, tracks.toArray(new Track[2]));
		artist= new Artist(artistName,new Album[]{album});
	}
	
	
	@Test
	public void shouldReturnJsonArtistsList() throws Exception {
		
		
		ArtistsRepository repository = mock(ArtistsRepository.class);
		when(repository.findAll()).thenReturn(Arrays.asList(artist));
		ArtistsController controller= new ArtistsController(repository);
		
		MockMvc mockMvc = standaloneSetup(controller).build();
		mockMvc.perform(get("/artists").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$[0].artistName").value("Metallica"))
			.andExpect(jsonPath("$[1].artistName").doesNotExist());
	}
	
	@Test //not working, why? Because you are moron and you did't create DEFAULT CONSTRUCTOR for Album, Track and Artist class
	public void shouldAddNewArtist() throws Exception {
		ArtistsRepository repository = mock(ArtistsRepository.class);
		
		Track michaelTrack= new Track("Loving You");
		Album michaelAlbum= new Album("XSCAPE", new Track[] {michaelTrack});
		
		Artist sendedArtist= new Artist("Michael Jackson",new Album[] {michaelAlbum});
		
		//na szybko tworzymy reprezentację obiektu w postaci JSONa
				Gson gson = new Gson();
				String json= gson.toJson(sendedArtist);
				
				System.out.println(json);
				
		when(repository.save(sendedArtist)).thenReturn(sendedArtist);
		
		ArtistsController controller= new ArtistsController(repository);
		
		//no i czemu znow zwraca mi 400 zamiast 200? :| 3 dni w dupę
		//bo jestes idiotą i nie stworzyłeś domyślnych konstruktorów.
		MockMvc mockMvc = standaloneSetup(controller).build();
		mockMvc.perform(
					post("/artists")
					.contentType(MediaType.APPLICATION_JSON)
					.content(json))
						.andExpect(status().isCreated())
						.andDo(print())
						.andExpect(jsonPath("$.artistName").value("Michael Jackson"));
	}
	
	@Test
	public void shouldReturnOneArtist() throws Exception {
		ArtistsRepository repository = mock(ArtistsRepository.class);
		when(repository.findOne(artist.getArtistName())).thenReturn(artist);
		
		ArtistsController controller = new ArtistsController(repository);
		
		MockMvc mvc = standaloneSetup(controller).build();
		
		mvc.perform(get("/artists/Metallica"))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.artistName").value("Metallica"));
		
	}
	@Test
	public void shouldReturnModifiedArtist() throws Exception {
		
		
		Artist modifiedArtist= new Artist();
		modifiedArtist.setAlbums(artist.getAlbums());
		
		modifiedArtist.setArtistName("Not Metallica");
		
		ArtistsRepository repository = mock(ArtistsRepository.class);
		
		when(repository.findOne("Metallica")).thenReturn(artist);
		doNothing().when(repository).delete(artist);
		when(repository.save(modifiedArtist)).thenReturn(modifiedArtist);
		
		ArtistsController controller = new ArtistsController(repository);
		
		MockMvc mvc = standaloneSetup(controller).build();
		
		Gson gson = new Gson();
		String json= gson.toJson(modifiedArtist);
		System.out.println(json);
		mvc.perform(put("/artists/Metallica").contentType(MediaType.APPLICATION_JSON).content(json))
			.andExpect(status().isOk())
			.andDo(print())
			.andExpect(jsonPath("$.artistName").value("Not Metallica"));
		
	}
	
	@Test
	public void shouldDeleteOneEntry() throws Exception{
		
		ArtistsRepository repository = mock(ArtistsRepository.class);
		
		when(repository.findOne("Metallica")).thenReturn(artist);
		doNothing().when(repository).delete(artist);
		
		ArtistsController controller = new ArtistsController(repository);
		
		MockMvc mvc = standaloneSetup(controller).build();
		
		mvc.perform(delete("/artists/Metallica").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andDo(print());
		
	}
	
}
