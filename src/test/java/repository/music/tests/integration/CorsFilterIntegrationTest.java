package repository.music.tests.integration;

import com.google.gson.Gson;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;
import repository.music.WebConfig;
import repository.music.configuration.MongoConfig;
import repository.music.configuration.SecurityConfig;
import repository.music.data.*;
import org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors;
import repository.music.security.CorsFilterConfig;

import java.util.ArrayList;
import java.util.List;

import static org.hamcrest.Matchers.hasSize;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.header;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * Created by aleksander on 08.03.17.
 */
//requests copied from angularjs web application
@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={SecurityConfig.class,WebConfig.class, MongoConfig.class, ArtistsRepositoryImpl.class})
public class CorsFilterIntegrationTest {


    private MockMvc mockMvc;

    @Autowired
    private ArtistsRepository repository;

    @Autowired
    private WebApplicationContext wac;

    @Before
    public void setUpDatabase() throws Exception {

        //stworzenie mockowanego mvc przed każdym testem
        mockMvc= MockMvcBuilders.webAppContextSetup(wac).
                apply(springSecurity())
                .build();
    }

    @Test
    public void testOptionsRequest() throws Exception {
        mockMvc.perform(options("/artists").header("Origin","http://localhost:8000").header("Access-Control-Request-Method", "GET"))
                .andExpect(status().isOk())
                .andExpect(header().string("Access-Control-Allow-Origin","*"))
                .andDo(print());
    }

    @Test
    public void testGetRequest() throws Exception {
        mockMvc.perform(get("/artists").header("Origin","http://localhost:8000"))
                .andExpect(status().isOk())
                .andExpect(header().string("Access-Control-Allow-Origin","*"))
                .andDo(print());
    }

    @Test
    public void testPostRequestWithValidCredentials() throws Exception {

        Artist newOne= new Artist("newOne", null);

        Gson gson = new Gson();
        String json= gson.toJson(newOne);


        //create
        mockMvc.perform(post("/artists").header("Origin","http://localhost:8000").with(httpBasic("user", "password")).contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(header().string("Access-Control-Allow-Origin","*"))
                .andExpect(status().isCreated());



        mockMvc.perform(delete("/artists/newOne").header("Origin","http://localhost:8000").with(httpBasic("user", "password")).contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(header().string("Access-Control-Allow-Origin","*"))
                .andExpect(status().isOk());

    }

    @Test
    public void testPostRequestWithInvalidCredentials() throws Exception {

        Artist newOne= new Artist("newInvalidOne", null);

        Gson gson = new Gson();
        String json= gson.toJson(newOne);


        //create
        mockMvc.perform(post("/artists").header("Origin","http://localhost:8000").with(httpBasic("user", "blahblahblah")).contentType(MediaType.APPLICATION_JSON).content(json))
                .andDo(print())
                .andExpect(header().string("Access-Control-Allow-Origin","*"))
                .andExpect(status().isUnauthorized());

    }

    @Test
    public void testGetRequestWithValidCredentials() throws Exception {

        mockMvc.perform(get("/artists").header("Origin","http://localhost:8000").with(httpBasic("user", "password")))
                .andExpect(status().isOk())
                .andExpect(header().string("Access-Control-Allow-Origin","*"))
                .andDo(print());
    }

    @Test
    public void testGetRequestWithInvalidCredentials() throws Exception {

        mockMvc.perform(get("/artists").header("Origin","http://localhost:8000").with(httpBasic("user", "blahblahblah")))
                .andExpect(status().isUnauthorized())
                .andExpect(header().string("Access-Control-Allow-Origin","*"))
                .andDo(print());
    }



    @After
    public void deleteCreatedEnities(){
        repository.delete("newOne");
    }

}
