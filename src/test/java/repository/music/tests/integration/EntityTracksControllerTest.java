package repository.music.tests.integration;
import static org.junit.Assert.*;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.hamcrest.Matchers.hasSize;
import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

import repository.music.WebConfig;
import repository.music.configuration.FongoConfig;
import repository.music.configuration.MongoConfig;
import repository.music.configuration.SecurityConfig;
import repository.music.data.Album;
import repository.music.data.Artist;
import repository.music.data.ArtistsRepository;
import repository.music.data.ArtistsRepositoryImpl;
import repository.music.data.Track;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={SecurityConfig.class,WebConfig.class, MongoConfig.class, ArtistsRepositoryImpl.class})
public class EntityTracksControllerTest {

private MockMvc mockMvc;
	
	@Autowired
	private ArtistsRepository repository;
	
	@Autowired
	private WebApplicationContext wac;
	
	
	@Before
	public void setUpDatabase(){
		
		List<Track> tracks;
		String albumTitle="Metallica";
		Album album;
		Artist artist;
		String artistName="Metallica";
		
		tracks= new ArrayList<>();
		Track temp= new Track("Nothing Else Matters");
		Track temp2= new Track("TheUnforgiven");
		tracks.add(temp);
		tracks.add(temp2);
		album= new Album(albumTitle, tracks.toArray(new Track[2]));
		artist= new Artist(artistName,new Album[]{album});
		
		Track michaelTrack= new Track("Chicago");
		Album michaelAlbum= new Album("XSCAPE", new Track[] {michaelTrack});
		
		Artist anotherArtist= new Artist("Michael Jackson",new Album[] {michaelAlbum});
		
		repository.save(artist);
		repository.save(anotherArtist);
		
		//stworzenie mockowanego mvc przed każdym testem
		mockMvc=MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
	}

	@After
	public void tearDown() throws Exception {
		repository.deleteAll();
	}

	@Test
	public void testGetTracks() throws Exception {
		
		mockMvc.perform(get("/artists/Metallica/albums/Metallica/tracks").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$").isArray());
	}

	@Test
	public void testAddTrack() throws Exception {
		
		Track track= new Track("New One");
		
		//na szybko tworzymy reprezentację obiektu w postaci JSONa
				Gson gson = new Gson();
				String json= gson.toJson(track);
		
		mockMvc.perform(post("/artists/Metallica/albums/Metallica/tracks").with(httpBasic("user","password")).contentType(MediaType.APPLICATION_JSON).content(json))
			.andExpect(status().isCreated())
			.andExpect(jsonPath("$.trackTitle").value("New One"));
		
		assertTrue(repository.findAlbumByArtistNameAndAlbumTitle("Metallica", "Metallica").getTracks().length==3);
		
		assertNotNull(repository.findTrackByArtistNameAndAlbumTitleAndTrackTitle("Metallica", "Metallica", "New One"));
	}

	@Test
	public void testGetTrack() throws Exception {
		mockMvc.perform(get("/artists/Metallica/albums/Metallica/tracks/TheUnforgiven").accept(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.trackTitle").value("TheUnforgiven"));
		
	}

	@Test
	public void testModifyTrack() throws Exception {
		Track track= new Track("New One");
		
		//na szybko tworzymy reprezentację obiektu w postaci JSONa
				Gson gson = new Gson();
				String json= gson.toJson(track);
		mockMvc.perform(put("/artists/Metallica/albums/Metallica/tracks/TheUnforgiven").with(httpBasic("user","password")).contentType(MediaType.APPLICATION_JSON).content(json))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.trackTitle").value("New One"));
		
			assertTrue(repository.findAlbumByArtistNameAndAlbumTitle("Metallica", "Metallica").getTracks().length==2);
			assertNotNull(repository.findTrackByArtistNameAndAlbumTitleAndTrackTitle("Metallica", "Metallica", "New One"));
	}

	@Test
	public void testDeleteTrack() throws Exception {
		
		mockMvc.perform(delete("/artists/Metallica/albums/Metallica/tracks/TheUnforgiven").with(httpBasic("user","password")).contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
		
			assertTrue(repository.findAlbumByArtistNameAndAlbumTitle("Metallica", "Metallica").getTracks().length==1);
	}
	
	@Test
	public void shouldReturn404Status() throws Exception {
		mockMvc.perform(delete("/artists/Metallica/albums/Metallica/tracks/TheUnforgiven"))
			.andExpect(status().isUnauthorized());
		mockMvc.perform(put("/artists/Metallica/albums/Metallica/tracks/TheUnforgiven").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isUnauthorized());
		mockMvc.perform(post("/artists/Metallica/albums/Metallica/tracks").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isUnauthorized());
	}

}
