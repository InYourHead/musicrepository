package repository.music.tests.integration;



import static org.hamcrest.Matchers.hasSize;
import static org.junit.Assert.assertTrue;
import static org.springframework.security.test.web.servlet.request.SecurityMockMvcRequestPostProcessors.httpBasic;
import static org.springframework.security.test.web.servlet.setup.SecurityMockMvcConfigurers.springSecurity;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.put;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

import java.util.ArrayList;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.context.WebApplicationContext;

import com.google.gson.Gson;

import repository.music.RootConfig;
import repository.music.WebConfig;
import repository.music.configuration.FongoConfig;
import repository.music.configuration.MongoConfig;
import repository.music.configuration.SecurityConfig;
import repository.music.data.Album;
import repository.music.data.Artist;
import repository.music.data.ArtistsRepository;
import repository.music.data.ArtistsRepositoryImpl;
import repository.music.data.Track;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringBootTest(classes={SecurityConfig.class,WebConfig.class, MongoConfig.class, ArtistsRepositoryImpl.class})
public class EntityArtistsControllerTest {

	private MockMvc mockMvc;
	
	@Autowired
	private ArtistsRepository repository;
	
	@Autowired
	private WebApplicationContext wac;
	
	
	@Before
	public void setUpDatabase(){
		
		List<Track> tracks;
		String albumTitle="Metallica";
		Album album;
		Artist artist;
		String artistName="Metallica";
		
		tracks= new ArrayList<>();
		Track temp= new Track("Nothing Else Matters");
		Track temp2= new Track("The unforgiven");
		tracks.add(temp);
		tracks.add(temp2);
		album= new Album(albumTitle, tracks.toArray(new Track[2]));
		artist= new Artist(artistName,new Album[]{album});
		
		Track michaelTrack= new Track("Loving You");
		Album michaelAlbum= new Album("XSCAPE", new Track[] {michaelTrack});
		
		Artist anotherArtist= new Artist("Michael Jackson",new Album[] {michaelAlbum});
		
		repository.save(artist);
		repository.save(anotherArtist);
		
		//stworzenie mockowanego mvc przed każdym testem
		mockMvc=MockMvcBuilders.webAppContextSetup(wac).apply(springSecurity()).build();
	}

	@After
	public void tearDown() throws Exception {
		repository.deleteAll();
	}

	@Test
	public void testGetArtists() throws Exception {
		mockMvc.perform(get("/artists").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$").isArray())
			.andExpect(jsonPath("$",hasSize(2)));
	}

	
	@Test
	public void testAddArtist() throws Exception {
		Track track= new Track("New One");
		Album album= new Album("I dont know", new Track[] {track});
		
		Artist artist= new Artist("George",new Album[] {album});
		
		//na szybko tworzymy reprezentację obiektu w postaci JSONa
				Gson gson = new Gson();
				String json= gson.toJson(artist);
		
		mockMvc.perform(post("/artists").with(httpBasic("user","password")).contentType(MediaType.APPLICATION_JSON).content(json))
			.andExpect(status().isCreated())
			.andDo(print())
			.andExpect(jsonPath("$").isNotEmpty());
				
		assertTrue(repository.count()==3);
				
	}

	@Test
	public void testGetArtist() throws Exception {
		mockMvc.perform(get("/artists/Metallica").contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.artistName").value("Metallica"));
					
	}

	@Test
	public void testModifyArtist() throws Exception {
		List<Track> tracks= new ArrayList<>();
		Track temp= new Track("Nothing Else Matters");
		Track temp2= new Track("The unforgiven");
		Track temp3= new Track("Whisky in the Jar");
		tracks.add(temp);
		tracks.add(temp2);
		tracks.add(temp3);
		Album album= new Album("Metallica", tracks.toArray(new Track[3]));
		Artist artist= new Artist("Metallica",new Album[]{album});
		
		Gson gson = new Gson();
		String json= gson.toJson(artist);
		
		mockMvc.perform(put("/artists/Metallica").with(httpBasic("user","password")).contentType(MediaType.APPLICATION_JSON).content(json))
			.andExpect(status().isOk())
			.andExpect(jsonPath("$.albums.[0].tracks", hasSize(3)));
		
		assertTrue(repository.findAllTracksByArtistNameAndAlbumTitle("Metallica", "Metallica").size()==3);
	}

	@Test
	public void testDeleteArtist() throws Exception {
		
		mockMvc.perform(delete("/artists/Metallica").with(httpBasic("user","password")).contentType(MediaType.APPLICATION_JSON))
			.andExpect(status().isOk());
		assertTrue(repository.count()==1);
	}
	
	@Test
	public void shouldReturn404Status() throws Exception {
		mockMvc.perform(delete("/artists/Metallica"))
			.andExpect(status().isUnauthorized());
		mockMvc.perform(put("/artists/Metallica").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isUnauthorized());
		mockMvc.perform(post("/artists").contentType(MediaType.APPLICATION_JSON))
		.andExpect(status().isUnauthorized());
	}

}
