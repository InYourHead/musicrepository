package repository.music.configuration;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

import org.springframework.security.web.authentication.www.BasicAuthenticationFilter;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.CorsConfigurationSource;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import repository.music.security.AllowOriginFilter;
import repository.music.security.RestAuthenticationEntryPoint;

import java.util.Arrays;

@Configuration
@EnableWebSecurity
@ComponentScan("repository.music.security")
public class SecurityConfig extends WebSecurityConfigurerAdapter{


	@Autowired
    private RestAuthenticationEntryPoint restAuthenticationEntryPoint;
	
	@Override
	protected void configure(AuthenticationManagerBuilder auth) throws Exception {
		auth.inMemoryAuthentication()
		//tak troszkę na sztywno, można by było dodać autoryzację na podstawie bazy danych
		.withUser("user").password("password").roles("USER")
		.and().withUser("admin").password("admin1")
		.roles("ADMIN");

	}
	
	@Override
	protected void configure(HttpSecurity http) throws Exception {

		//http.addFilterBefore(new AllowOriginFilter(), BasicAuthenticationFilter.class);
		http.csrf().disable()
				.cors().and()
		.authorizeRequests()
		//ciekawe, jak się da tylko */ to nie działa...
			.antMatchers(HttpMethod.GET, "/**").permitAll()
			.antMatchers(HttpMethod.OPTIONS,"/**").permitAll()
				.anyRequest().authenticated()
				.and()
			.httpBasic().and().exceptionHandling().authenticationEntryPoint(restAuthenticationEntryPoint);
		/*
		
		 *.antMatchers(HttpMethod.GET,"/*" ).denyAll().and()
		.authorizeRequests().antMatchers(HttpMethod.POST, "/*").authenticated()
			.and().httpBasic().and()
		.authorizeRequests().antMatchers(HttpMethod.DELETE, "/*").authenticated()
			.and().httpBasic().and()
		.authorizeRequests().antMatchers(HttpMethod.PUT, "/*").authenticated()
			.and().httpBasic();/
			*/
	}
	@Bean
	CorsConfigurationSource corsConfigurationSource() {
		CorsConfiguration configuration = new CorsConfiguration();
		configuration.addAllowedHeader("*");
		configuration.addAllowedOrigin("*" /*Arrays.asList("http://localhost:8000")*/);
		configuration.setAllowedMethods(Arrays.asList("GET","POST", "DELETE", "PUT", "OPTIONS"));
		UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
		source.registerCorsConfiguration("/**", configuration);
		return source;
	}

}
