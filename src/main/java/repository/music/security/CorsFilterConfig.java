package repository.music.security;

import org.springframework.boot.web.servlet.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.cors.CorsConfiguration;
import org.springframework.web.cors.UrlBasedCorsConfigurationSource;
import org.springframework.web.filter.CorsFilter;
import org.springframework.web.filter.OncePerRequestFilter;

import java.util.Arrays;

/**
 * Created by aleksander on 21.02.17.
 */
//@Configuration
public class CorsFilterConfig{

   /* @Bean(name = "corsFilter")
    public FilterRegistrationBean corsFilter() {
        UrlBasedCorsConfigurationSource source = new UrlBasedCorsConfigurationSource();
        CorsConfiguration config = new CorsConfiguration();
        config.setAllowCredentials(true);
        config //TODO set real domain address
                .addAllowedOrigin("http://localhost:8000");
        config.addAllowedHeader("*");
        //config.addAllowedMethod("*");
        config.setAllowedHeaders(Arrays.asList("PUT", "DELETE",
                "GET", "POST", "OPTIONS", "PATCH"));
        source.registerCorsConfiguration("/**", config);
        FilterRegistrationBean bean = new FilterRegistrationBean(new CorsFilter(source));
        bean.setOrder(0);
        return bean;
    }
/*
    @Bean
    public FilterRegistrationBean allowOriginRegistrationBean() {

        FilterRegistrationBean registration = new FilterRegistrationBean();
        registration.setFilter(allowOriginFilter());
        registration.addUrlPatterns("/**");
        registration.setOrder(0);
        return registration;
    }

    @Bean(name = "allowOriginFilter")
    public OncePerRequestFilter allowOriginFilter() {
        return new AllowOriginFilter();
    }
*/
}