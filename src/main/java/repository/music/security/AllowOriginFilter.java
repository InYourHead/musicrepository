package repository.music.security;

import org.springframework.boot.autoconfigure.jersey.JerseyProperties;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.filter.OncePerRequestFilter;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.xml.crypto.dsig.spec.XPathType;
import java.io.IOException;

/**
 * Created by aleksander on 07.03.17.
 */
@Configuration
public class AllowOriginFilter extends OncePerRequestFilter{



    @Override
    protected void doFilterInternal(HttpServletRequest httpServletRequest, HttpServletResponse httpServletResponse, FilterChain filterChain) throws ServletException, IOException {

            filterChain.doFilter(httpServletRequest, httpServletResponse);

        if(httpServletResponse.getHeader("Access-Control-Allow-Origin")==null || httpServletResponse.getHeader("Access-Control-Allow-Origin").isEmpty()) httpServletResponse.addHeader("Access-Control-Allow-Origin", "http://localhost:8000");


    }
}
