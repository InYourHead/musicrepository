package repository.music.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import repository.music.configuration.exceptions.ResourceAlreadyExistsException;
import repository.music.configuration.exceptions.ResourceNotFoundException;
import repository.music.data.ArtistsRepository;
import repository.music.data.Track;

@RestController
@RequestMapping("/artists/{artistName}/albums/{albumTitle}/tracks")
public class TracksController {

	@Autowired
	ArtistsRepository repository;
	
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(method = RequestMethod.GET, produces = "application/json")
	public List<Track> getTracks(@PathVariable String artistName, @PathVariable String albumTitle){
		List<Track> result= repository.findAllTracksByArtistNameAndAlbumTitle(artistName, albumTitle);
			if(result==null)
				throw new ResourceNotFoundException();
			return result;
	}
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public Track addTrack(@PathVariable String artistName, @PathVariable String albumTitle, @RequestBody Track newTrack){
		Track result= repository.findTrackByArtistNameAndAlbumTitleAndTrackTitle(artistName, albumTitle, newTrack.getTrackTitle());
			if(result!=null)
				throw new ResourceAlreadyExistsException();
			return repository.findAlbumByArtistNameAndAlbumTitleAndInsertTrack(artistName, albumTitle, newTrack);
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value="/{trackTitle}", method= RequestMethod.GET, produces= "application/json")
	public Track getTrack(@PathVariable String artistName, @PathVariable String albumTitle, @PathVariable String trackTitle){
		Track result = repository.findTrackByArtistNameAndAlbumTitleAndTrackTitle(artistName, albumTitle, trackTitle);
			if(result==null) 
				throw new ResourceNotFoundException();
			return result;
	}

	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value="/{trackTitle}", method= RequestMethod.PUT, consumes= "application/json", produces= "application/json")
	public Track modifyTrack(@PathVariable String artistName, @PathVariable String albumTitle, @PathVariable String trackTitle, @RequestBody Track modifiedTrack){
		Track queryResult = repository.findTrackByArtistNameAndAlbumTitleAndTrackTitle(artistName, albumTitle, trackTitle);
			if(queryResult==null) 
				throw new ResourceNotFoundException();
			Track result=repository.findTrackByArtistNameAndAlbumTitleAndTrackTitileAndUpdate(artistName, albumTitle, trackTitle, modifiedTrack);
	return result;
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value="/{trackTitle}", method= RequestMethod.DELETE, produces= "application/json")
	public Track deleteTrack(@PathVariable String artistName, @PathVariable String albumTitle, @PathVariable String trackTitle){
		Track queryResult = repository.findTrackByArtistNameAndAlbumTitleAndTrackTitle(artistName, albumTitle, trackTitle);
			if(queryResult==null) 
				throw new ResourceNotFoundException();
			repository.findTrackByArtistNameAndAlbumTitleAndTrackTitileAndDelete(artistName, albumTitle, trackTitle);
			return queryResult;
	}
}
