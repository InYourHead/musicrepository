package repository.music.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import repository.music.configuration.exceptions.ResourceAlreadyExistsException;
import repository.music.configuration.exceptions.ResourceNotFoundException;
import repository.music.data.Album;
import repository.music.data.ArtistsRepository;

@RestController
@RequestMapping("/artists/{artistName}/albums")
public class AlbumsController {

	ArtistsRepository repository;
	
	@Autowired
	public AlbumsController(ArtistsRepository repository) {
		this.repository=repository;
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(method= RequestMethod.GET, produces = "application/json")
	public List<Album> getAlbums(@PathVariable String artistName) {
		return repository.findAllAlbumsByArtistName(artistName);
	}
	
	@ResponseStatus(code = HttpStatus.CREATED)
	@RequestMapping(method= RequestMethod.POST, consumes= "application/json",produces = "application/json")
	public Album addAlbum(@PathVariable String artistName, @RequestBody Album album) {
		
		if(repository.findArtistByArtistName(artistName)==null)
			throw new ResourceNotFoundException();
		
		if(repository.findAlbumByArtistNameAndAlbumTitle(artistName, album.getAlbumTitle())!=null)
			throw new ResourceAlreadyExistsException();
		
		repository.findArtistByArtistNameAndInsertAlbum(artistName, album);
		
	 return album;
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value ="/{albumTitle}",method = RequestMethod.GET, produces = "application/json")
	public Album getAlbum(@PathVariable String artistName, @PathVariable String albumTitle) {
		Album result=repository.findAlbumByArtistNameAndAlbumTitle(artistName, albumTitle);
		if(result==null)
			throw new ResourceNotFoundException();
		return result;	
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value= "/{albumTitle}", method = RequestMethod.PUT, consumes = "application/json", produces ="application/json")
	public Album modifyAlbum(@PathVariable String artistName, @PathVariable String albumTitle, @RequestBody(required = true) Album modifiedAlbum) {
		
		if(repository.findAlbumByArtistNameAndAlbumTitle(artistName, albumTitle)==null)
			throw new ResourceNotFoundException();
		//czemu, jak to zwracam, to się zjebuje i nie działa (zwraca empty json)?
		repository.findAlbumByArtistNameAndAlbumTitleAndUpdate(artistName, albumTitle, modifiedAlbum);
		
		return modifiedAlbum;
	}
	
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value= "/{albumTitle}", method = RequestMethod.DELETE,produces ="application/json")
	public Album deleteAlbum(@PathVariable String artistName, @PathVariable String albumTitle) {
		Album queryResult= repository.findAlbumByArtistNameAndAlbumTitle(artistName, albumTitle);
		if(queryResult==null)
			throw new ResourceNotFoundException();
		
		repository.deleteAlbumByArtistNameAndAlbumTitle(artistName, albumTitle);
		
		return queryResult;
	}
}
