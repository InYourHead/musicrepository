package repository.music.web;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import repository.music.configuration.exceptions.ResourceAlreadyExistsException;
import repository.music.configuration.exceptions.ResourceNotFoundException;
import repository.music.data.Artist;
import repository.music.data.ArtistsRepository;

//dzięki tej adnotacji nie trzeba dodawać RequestBody i RequestResponse
@RestController
@RequestMapping("/artists")
public class ArtistsController {

	
	ArtistsRepository repository;
	
	@Autowired
	public ArtistsController(ArtistsRepository repository) {
		this.repository=repository;
	}
	
	//return all existing Artists
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(method=RequestMethod.GET, produces = "application/json")
	public List<Artist> getArtists() {
		return repository.findAll();
	}
	
	//adding new artist
	@ResponseStatus(code = HttpStatus.CREATED)
	@RequestMapping(method=RequestMethod.POST, consumes = "application/json")
	public Artist addArtist(@RequestBody(required= true)  Artist artist){
		
		if(repository.findOne(artist.getArtistName())!=null)
			throw new ResourceAlreadyExistsException();
		
		repository.save(artist);
		return artist;
	}
	
	//return selected artist
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value="/{artistName}", method=RequestMethod.GET, produces = "application/json")
	public Artist getArtist(@PathVariable("artistName") String artistName) {
		
		Artist result= repository.findOne(artistName);
		
		if(result==null) throw new ResourceNotFoundException();
		return result;
	}
	
	//modify selected artist
	@ResponseStatus(code = HttpStatus.OK)
	@RequestMapping(value="/{artistName}", method=RequestMethod.PUT, consumes= "application/json", produces = "application/json")
	public Artist modifyArtist(@PathVariable("artistName") String artistName, @RequestBody(required= true) Artist modifiedArtist) {
		
		Artist result= repository.findOne(artistName);
		
		if(result==null) 
			throw new ResourceNotFoundException();
		
		repository.delete(result);
		repository.save(modifiedArtist);
		
		return modifiedArtist;
	}
	
	//delete selected artist
	@ResponseStatus(code= HttpStatus.OK)
	@RequestMapping(value="/{artistName}", method=RequestMethod.DELETE, produces="application/json")
	public void deleteArtist(@PathVariable("artistName") String artistName) {
		
		Artist result = repository.findOne(artistName);
		
		if(result==null) 
			throw new ResourceNotFoundException();
		
		repository.delete(result);
	}
}
