package repository.music.data;

import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Track {
	
	private String trackTitle;

	public Track(){
		
	}
	
	public Track(String trackTitle) {
		this.trackTitle = trackTitle;
	}

	public String getTrackTitle() {
		return trackTitle;
	}

	public void setTrackTitle(String trackTitle) {
		this.trackTitle = trackTitle;
	}
	

}
