package repository.music.data;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Criteria;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;
import com.mongodb.BasicDBObject;

@Repository
public class ArtistsRepositoryImpl implements ArtistsRepositoryCustomQueries {

	
	@Autowired
	private MongoOperations mongo;
	
	@Override
	public Album findArtistByArtistNameAndInsertAlbum(String artistName, Album album) {
		Update update = new Update();
		update.addToSet("albums", album);
		Criteria criteria = Criteria.where("_id").is(artistName);
		mongo.updateFirst(Query.query(criteria), update, Artist.class); //propably
		return album;
		
	}

	@Override
	public Album findAlbumByArtistNameAndAlbumTitleAndUpdate(String artistName, String albumTitle,
			Album modifiedAlbum) {
		Update update = new Update();
		update.set("albums.$.albumTitle", modifiedAlbum.getAlbumTitle());
		update.set("albums.$.tracks", modifiedAlbum.getTracks());
		//Fixed!
		Criteria criteria= Criteria.where("_id").is(artistName).andOperator(Criteria.where("albums.albumTitle").is(albumTitle));
		mongo.updateMulti(Query.query(criteria), update, Artist.class);

		return modifiedAlbum;
	}

	@Override
	public void deleteAlbumByArtistNameAndAlbumTitle(String artistName, String albumTitle) {
		
		Criteria criteria= Criteria.where("_id").is(artistName).andOperator(Criteria.where("albums.albumTitle").is(albumTitle));
		
		Update update= new Update().pull("albums", new BasicDBObject("albumTitle", albumTitle));
		
		mongo.updateFirst(Query.query(criteria), update, Artist.class);
	}

	//fixed!
	@Override
	public Track findAlbumByArtistNameAndAlbumTitleAndInsertTrack(String artistName, String albumTitle,
			Track newTrack) {
		
		
		Update update = new Update();

		Album album=null;
		List<Album> albumArray=this.findAllAlbumsByArtistName(artistName);

		System.out.println("AlbumArray: " + albumArray.size());

		for(Album a : albumArray ){

			if(a.getAlbumTitle().equals(albumTitle)){

				album=a;
				List<Track> oldTracks= new ArrayList<>(Arrays.asList(album.getTracks()));
				oldTracks.add(newTrack);

				album.setTracks(oldTracks.toArray(new Track[oldTracks.size()]));
				break;
			}
		}
		List<Album> newAlbumArray= new ArrayList<>();

		for(Album a: albumArray){
			if(a.getAlbumTitle().equals(albumTitle)){
				newAlbumArray.add(album);
			}
			else
				newAlbumArray.add(a);
		}

		update.set("albums", newAlbumArray);

		Criteria criteria = Criteria.where("_id").is(artistName);

		mongo.updateFirst(Query.query(criteria), update, Artist.class);

		return newTrack;
		/*

		Album album= this.findAlbumByArtistNameAndAlbumTitle(artistName, albumTitle);
		
		List<Track> oldTracks= new ArrayList<>(Arrays.asList(album.getTracks()));
		oldTracks.add(newTrack);
		
		album.setTracks(oldTracks.toArray(new Track[oldTracks.size()]));

		update.addToSet("albums", album);

		Criteria criteria = Criteria.where("_id").is(artistName).andOperator(Criteria.where("albums.$.albumTitle").is(albumTitle));
		//this.findAlbumByArtistNameAndAlbumTitleAndUpdate(artistName, albumTitle, album);

		mongo.updateMulti(Query.query(criteria), update, Album.class);


		return newTrack;
*/

	}

	@Override
	public Track findTrackByArtistNameAndAlbumTitleAndTrackTitileAndUpdate(String artistName, String albumTitle,
			String trackTitle, Track modifiedTrack) {
			
						
			Album album=this.findAlbumByArtistNameAndAlbumTitle(artistName, albumTitle);
			int index=0;
			for(int i=0; i<album.getTracks().length; i++)
				if(album.getTracks()[i].getTrackTitle().equals(trackTitle))
				{
					index=i;
					break;
				}
				
				
			Update update = new Update();
			//nie może być dwóch znaków $, taka ciekawostka
			update.set("albums.$.tracks."+ Integer.toString(index) +".trackTitle", modifiedTrack.getTrackTitle());
			System.out.println("albums.$.tracks."+ Integer.toString(index) +".trackTitle");
			
			Criteria criteria= Criteria.where("_id").is(artistName).andOperator(Criteria.where("albums.albumTitle").is(albumTitle).andOperator(Criteria.where("albums.tracks.trackTitle").is(trackTitle)));
			mongo.updateMulti(Query.query(criteria), update, Artist.class);
			
			return modifiedTrack;
	}

	@Override
	public void findTrackByArtistNameAndAlbumTitleAndTrackTitileAndDelete(String artistName, String albumTitle,
			String trackTitle) {
		
		Criteria criteria= Criteria.where("_id").is(artistName).andOperator(Criteria.where("albums.albumTitle").is(albumTitle).andOperator(Criteria.where("albums.tracks.trackTitle").is(trackTitle)));
		
		Update update= new Update().pull("albums.$.tracks", new BasicDBObject("trackTitle", trackTitle));
		
		mongo.updateMulti(Query.query(criteria), update, Artist.class);
		
		
	}

	@Override
	public Album findAlbumByArtistNameAndAlbumTitle(String artistName, String albumTitle) {

		Artist artist= mongo.findOne(Query.query(Criteria.where("_id").is(artistName)), Artist.class);	
		
	for (Album album : artist.getAlbums()) {
		if(album.getAlbumTitle().equals(albumTitle))
			return album;
		
	}
		return null;
		
	}

	@Override
	public List<Album> findAllAlbumsByArtistName(String artistName) {
		
		
		Artist artist= mongo.findOne(Query.query(Criteria.where("_id").is(artistName)), Artist.class);
		
		
		return new ArrayList<Album>(Arrays.asList(artist.getAlbums()));
	}
	
	@Override
	public List<Track> findAllTracksByArtistNameAndAlbumTitle(String artistName, String albumTitle) {
		
		Artist artist= mongo.findOne(Query.query(Criteria.where("_id").is(artistName)), Artist.class);	
		
		for (Album album : artist.getAlbums()) {
			if(album.getAlbumTitle().equals(albumTitle))
				return Arrays.asList(album.getTracks());
			
		}
		return null;
	}

	@Override
	public Track findTrackByArtistNameAndAlbumTitleAndTrackTitle(String artistName, String albumTitle,
			String trackTitle) {

		Artist artist= mongo.findOne(Query.query(Criteria.where("_id").is(artistName)), Artist.class);	
		
		for (Album album : artist.getAlbums()) {
			if(album.getAlbumTitle().equals(albumTitle))
				for(Track track : album.getTracks())
					if(track.getTrackTitle().equals(trackTitle))
						return track;
			
		}
		return null;			
	}

}
