package repository.music.data;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;
import javax.persistence.ElementCollection;

@Document
public class Artist {
	
	@Id
	private String artistName;
	
	@ElementCollection
	private Album [] albums;

	public Artist(){
		
	}
	
	public Artist(String artistName, Album[] albums) {
		this.artistName=artistName;
		this.albums=albums;
	}
	
	public String getArtistName() {
		return artistName;
	}

	public void setArtistName(String artistName) {
		this.artistName = artistName;
	}

	public Album[] getAlbums() {
		return albums;
	}

	public void setAlbums(Album[] albums) {
		this.albums = albums;
	}
	
}
