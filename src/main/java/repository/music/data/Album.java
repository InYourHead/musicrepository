package repository.music.data;

import javax.persistence.ElementCollection;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document
public class Album {
	
	@Id
	private String albumTitle;
	
	@ElementCollection
	private Track [] tracks;
	
	public Album(){
		
	}
	
	public Album(String albumTitle, Track[] tracks) {
		this.albumTitle = albumTitle;
		this.tracks = tracks;
	}

	public String getAlbumTitle() {
		return albumTitle;
	}

	public void setAlbumTitle(String albumTitle) {
		this.albumTitle = albumTitle;
	}

	public Track[] getTracks() {
		return tracks;
	}

	public void setTracks(Track[] tracks) {
		this.tracks = tracks;
	}

}
