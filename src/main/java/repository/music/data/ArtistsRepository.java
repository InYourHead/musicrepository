package repository.music.data;

import java.util.List;

import org.springframework.data.mongodb.repository.MongoRepository;

public interface ArtistsRepository extends MongoRepository<Artist, String>, ArtistsRepositoryCustomQueries{

	public List<Artist> findAll();
	public Artist findArtistByArtistName(String artistName);
}
