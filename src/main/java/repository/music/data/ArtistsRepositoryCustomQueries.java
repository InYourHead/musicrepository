package repository.music.data;

import java.util.List;

public interface ArtistsRepositoryCustomQueries {
	

	public Album findArtistByArtistNameAndInsertAlbum(String artistName,Album album);
	
	public Album findAlbumByArtistNameAndAlbumTitleAndUpdate(String artistName,String albumTitle,Album modifiedAlbum);
	
	public void deleteAlbumByArtistNameAndAlbumTitle(String artistName,String albumTitle);
	
	public Track findAlbumByArtistNameAndAlbumTitleAndInsertTrack(String artistName,String albumTitle,Track newTrack);
	
	public Track findTrackByArtistNameAndAlbumTitleAndTrackTitileAndUpdate(String artistName,String albumTitle,String trackTitle,Track modifiedTrack);
	
	public void findTrackByArtistNameAndAlbumTitleAndTrackTitileAndDelete(String artistName,String albumTitle,String trackTitle);

	public Album findAlbumByArtistNameAndAlbumTitle(String artistName, String albumTitle);
	
	public List<Album> findAllAlbumsByArtistName(String artistName); 
	//gdybym chciał tutaj zwracać obiekt po aktualizacji, to nie wiem czemu wywala mi empty json, nawet, gdy zamockuje odpowiednią odpowiedź po wywołaniu funkcji...
	public List<Track> findAllTracksByArtistNameAndAlbumTitle(String artistName, String albumTitle);
	
	public Track findTrackByArtistNameAndAlbumTitleAndTrackTitle(String artistName, String albumTitle, String trackTitle);
}
