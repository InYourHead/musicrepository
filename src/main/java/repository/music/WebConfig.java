package repository.music;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.CorsRegistry;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurerAdapter;

@Configuration
@EnableWebMvc
@ComponentScan(basePackages= "repository.music.web")
public class WebConfig extends WebMvcConfigurerAdapter{

/*
    @Override
    public void addCorsMappings(CorsRegistry registry)
    {
        registry.addMapping("/**").allowedMethods("PUT", "DELETE",
                "GET", "POST", "OPTIONS", "PATCH").allowCredentials(true).allowedOrigins("http://localhost:8000");

    }
*/
	
}
